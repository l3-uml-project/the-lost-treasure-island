package TheLostTreasureIsland;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * Tile of the game board, is abstract because it only contains the basic information of the tile but does not have any type.
 */
abstract class Tile {
    /** X coordinate of the tile */
    int xCoord;

    /** Y coordinate of the tile */
    int yCoord;

    /** Icon of the tile */
    Image icon;

    /** Icon of the tile when in fog of war */
    Image foggedIcon;

    /** Boolean referencing if the tile is walkable or not */
    boolean isWalkable = true;

    /** Boolean referencing if the tile is diggable or not */
    boolean isDiggable = false;

    /** Boolean referencing if the tile is already dug or not */
    boolean isDug = false;

    /** Boolean referencing if the tile has the treasure or not */
    boolean hasTreasure = false;

    /** Boolean referencing if a person died in this tile or not */
    boolean hasSkull = false;

    /** Object currently on the tile */
    GameObject object;

    /** Person currently on the tile */
    Person person;

    /** Tile's abstract constructor */
    Tile(int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    /**
     * Used to determine if the tile is available to spawn an object or person on or not
     * @return  True if neither an object nor a person is on the tile, false otherwise.
     */
    boolean isAvailableForSpawn() {
        return person == null && object == null && !this.getClass().getName().equals(WaterTile.class.getName());
    }

    /**
     * Empty method that allows the player to dig to try to find the treasure.
     * @return  If the tile is diggable or not.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    boolean dig() throws InterruptedException {
        return isDiggable;
    }

    /**
     * Gets all the tiles in a specified range of the current's one.
     * @param range     Range in which to search for tiles.
     * @return  A list of tiles.
     */
    List<Tile> getTilesInRange(int range) {
        List<Tile> tilesInRange = new ArrayList<>();
        int numberOfRaws = 2 * range + 1;
        int numberOfColumns = 2 * range + 1;

        for (int currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
            for (int currentRaw = 0; currentRaw < numberOfRaws; currentRaw++) {
                int currentX = Math.floorMod((xCoord + currentRaw - range), GameBoard.numberOfTilesX);
                int currentY = Math.floorMod((yCoord + currentColumn - range), GameBoard.numberOfTilesY);
                tilesInRange.add(GameBoard.map[currentY][currentX]);
            }
        }

        return tilesInRange;
    }

    /**
     * Displays a visible tile and all the information it contains (skull, object, persons).
     */
    void display() {
        GameBoard.GC.drawImage(icon, xCoord * GameBoard.TILE_SIZE, yCoord * GameBoard.TILE_SIZE);
        if (hasSkull)
            GameBoard.GC.drawImage(Icon.skull, xCoord * GameBoard.TILE_SIZE, yCoord * GameBoard.TILE_SIZE);
        if (object != null)
            object.display();
        if (person != null)
            person.display();
    }

    /**
     * Displays a tile that is not visible (in the fog of war) with a fogged tile icon.
     */
    void displayFogged() {
        GameBoard.GC.drawImage(foggedIcon, xCoord * GameBoard.TILE_SIZE, yCoord * GameBoard.TILE_SIZE);
    }
}
