package TheLostTreasureIsland;

import java.io.*;

class DataManager {
    static void saveToFile(Object object, final String SAVES_PATH, String saveName) throws IOException {
        // Check if the save directory exists, and create it only if NOT
        File directory = new File(SAVES_PATH);
        if (! directory.exists()){
            directory.mkdirs();
        }

        // Save the data in the file
        OutputStream outStream = new FileOutputStream(SAVES_PATH + "/" + saveName + ".ser");
        ObjectOutputStream fileObjectOut = new ObjectOutputStream(outStream);
        fileObjectOut.writeObject(object);
        fileObjectOut.close();
        outStream.close();
    }

    static Object loadFromFile(String SAVES_PATH, String saveName) throws IOException, ClassNotFoundException {
        // Load the data in the file
        InputStream inStream;

        try {
            inStream = new FileInputStream(SAVES_PATH + "/" + saveName + ".ser");
        }
        catch (FileNotFoundException e) {
            System.out.println("File " + SAVES_PATH + "/" + saveName + ".ser not found.");
            return null;
        }

        ObjectInputStream fileObjectIn = new ObjectInputStream(inStream);
        Object object = fileObjectIn.readObject();
        fileObjectIn.close();
        inStream.close();

        return object;
    }
}
