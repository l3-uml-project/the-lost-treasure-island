package TheLostTreasureIsland;

import javafx.scene.image.Image;

/**
 * Static icon class referencing all the icons of the game.
 */
class Icon {
    /** Icon of a sand tile */
    static Image sand = new Image("file:icons/sand.png");

    /** Icon of a dug sand tile */
    static Image sandDug = new Image("file:icons/sand_dug.png");

    /** Icon of a sand tile which is in the fog of war */
    static Image sandFog = new Image("file:icons/sand_fog.png");

    /** Icon of a forest tile */
    static Image forest = new Image("file:icons/forest.png");

    /** Icon of a dug forest tile */
    static Image forestDug = new Image("file:icons/forest_dug.png");

    /** Icon of a forest tile which is in the fog of war */
    static Image forestFog = new Image("file:icons/forest_fog.png");

    /** Icon of a water tile */
    static Image water = new Image("file:icons/water.png");

    /** Icon of a water tile which is in the fog of war */
    static Image waterFog = new Image("file:icons/water_fog.png");

    /** Array of corsair icons */
    static Image[] corsairs = new Image[8];

    /** Icon of a buccaneer */
    static Image buccaneer = new Image("file:icons/buccaneer.png");

    /** Icon of a filibuster */
    static Image filibuster = new Image("file:icons/filibuster.png");

    /** Icon of a shovel */
    static Image shovel = new Image("file:icons/shovel.png");

    /** Icon of a machete */
    static Image machete = new Image("file:icons/machete.png");

    /** Icon of a musket */
    static Image musket = new Image("file:icons/musket.png");

    /** Icon of an armor */
    static Image armor = new Image("file:icons/armor.png");

    /** Icon of a skull */
    static Image skull = new Image("file:icons/skull.png");

    /** GameOver icon */
    static Image gameOver = new Image("file:icons/game_over.png");

    /**
     * Fills the 'corsairs' array with all the corsair icons.
     */
    static void init() {
        corsairs[0] = new Image("file:icons/corsair_1.png");
        corsairs[1] = new Image("file:icons/corsair_2.png");
        corsairs[2] = new Image("file:icons/corsair_3.png");
        corsairs[3] = new Image("file:icons/corsair_4.png");
        corsairs[4] = new Image("file:icons/corsair_5.png");
        corsairs[5] = new Image("file:icons/corsair_6.png");
        corsairs[6] = new Image("file:icons/corsair_7.png");
        corsairs[7] = new Image("file:icons/corsair_8.png");
    }
}
