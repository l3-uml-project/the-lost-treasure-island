package TheLostTreasureIsland;

/**
 * Filibuster (AI).
 */
class Filibuster extends AI {
    /** Number of created filibusters. Increments by 1 every time a filibuster is created */
    private static int filibusterID = 1;

    /**
     * Filibuster's constructor.
     * @param tile  Tile the filibuster is currently on
     */
    Filibuster(Tile tile) {
        super(tile);

        id = filibusterID;
        filibusterID++;

        pseudo = "Filibuster " + id;

        hasMusket = true;
        recalculateVictoryPercentage(this);

        moveRange = 1;
        attackRange = 1;

        icon = Icon.filibuster;
    }
}
