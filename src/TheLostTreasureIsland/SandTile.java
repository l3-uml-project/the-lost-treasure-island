package TheLostTreasureIsland;

/**
 * Sand tile.
 */
class SandTile extends Tile {
    /**
     * Sand tile's constructor.
     * @param xCoord    X coordinate of the tile.
     * @param yCoord    Y coordinate of the tile.
     */
    SandTile(int xCoord, int yCoord) {
        super(xCoord, yCoord);

        icon = Icon.sand;
        foggedIcon = Icon.sandFog;

        isWalkable = true;
        isDiggable = true;
    }

    /**
     * Called when a player try to dig on this tile. Update the tile's icon.
     * @return  True if the treasure was found, false otherwise.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    boolean dig() throws InterruptedException {
        if (!isDug) {
            icon = Icon.sandDug;
            if (hasTreasure) {
                Launcher.win("You found the treasure !", (Corsair) this.person);
                return true;
            }
        }
        return false;
    }
}
