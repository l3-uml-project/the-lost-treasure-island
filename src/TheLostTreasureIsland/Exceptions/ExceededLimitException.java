package TheLostTreasureIsland.Exceptions;

/**
 * Thrown when the applications args are exceeding a certain limit.
 */
public class ExceededLimitException extends Exception {
    /**
     * ExceededLimitException's constructor.
     * @param currentArgument   Argument on which the limits have been exceeded
     * @param number            Exceeding number
     * @param limit             Limits of the argument
     */
    public ExceededLimitException(String currentArgument, int number, int limit) {
        System.out.println("Limit exceeded for argument \"" + currentArgument + "\", set to \"" + number + "\" (limit is " + limit + ").");
    }
}
