package TheLostTreasureIsland.Exceptions;

/**
 * Thrown when an incorrect value is specified to one of the game's parameters in the applications args.
 */
public class UnsupportedValueOfArgumentException extends Exception {
    /**
     * UnsupportedValueOfArgumentException's constructor.
     * @param currentArgument   Argument on which the incorrect value have been specified
     * @param unsupportedValue  Problematic value
     */
    public UnsupportedValueOfArgumentException(String currentArgument, String unsupportedValue) {
        System.out.println("Unsupported value of \"" + currentArgument + "\": \"" + unsupportedValue + "\".");
    }
}
