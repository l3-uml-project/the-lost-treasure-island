package TheLostTreasureIsland.Exceptions;

/**
 * Thrown when one of the game parameters is not specified in the applications args.
 */
public class MissingArgumentException extends Exception {
    /**
     * MissingArgumentException's constructor.
     * @param missingArgument   Argument that is missing
     */
    public MissingArgumentException(String missingArgument) {
        System.out.println("Missing argument: " + missingArgument);
    }
}
