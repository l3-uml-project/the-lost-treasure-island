package TheLostTreasureIsland;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Game board of the current game, containing all the necessary information to run the game.
 */
class GameBoard implements java.io.Serializable {
    /** Used to issue draw calls to the Canvas */
    static GraphicsContext GC;

    /** Tile size in pixels */
    static int TILE_SIZE = 32;

    /** Number of tiles per raw */
    static int numberOfTilesX;

    /** Number of tiles per column */
    static int numberOfTilesY;

    /** Map of the game */
    static Tile[][] map;

    /** Percentage of water on the map */
    private static double waterPercentage = 5;

    /** Percentage of forest on the map */
    private static double forestPercentage = 5;

    /** Current turn in the game */
    private static int turn = 1;

    /** Boolean referencing if it's the player's turn or not */
    static boolean playerTurn = false;

    /** Boolean referencing if the keyboard events should be interpreted by the game or not */
    static boolean listenToUserInput = false;

    /** Range of the fog of war */
    private static int fogOfWarRange = -1;

    /** Boolean referencing of the fog of war is enabled or not this game */
    private static boolean fogOfWarIsEnabled = true;

    /** Current corsair (player) to play */
    private static Corsair currentPlayer;

    /** Current player index in the list */
    private static int currentPlayerIndex = 0;

    /** List of all the corsairs (players) in the game */
    private static List<Corsair> playerList = new ArrayList<>();

    /** List of all the buccaneers in the game */
    private static List<Buccaneer> buccaneerList = new ArrayList<>();

    /** List of all the filibusters in the game */
    private static List<Filibuster> filibusterList = new ArrayList<>();

    /** Minimum random value that can be used to reference a tile x or y coordinate */
    private static int randMin;

    /** Maximum random value that can be used to reference a tile x coordinate */
    private static int randMaxX;

    /** Maximum random value that can be used to reference a tile y coordinate */
    private static int randMaxY;

    /**
     * GameBoard's constructor.
     * @param GC                    GraphicsContext used to issue draw calls to the Canvas
     * @param numberOfTilesX        Number of tiles per raw
     * @param numberOfTilesY        Number of tiles per column
     * @param numberOfCorsairs      Number of corsairs (players) at the start of the game
     * @param numberOfBuccaneers    Number of buccaneers at the start of the game
     * @param numberOfFilibusters   Number of filibusters at the start of the game
     * @param fogOfWarRange         Range of the fog of war
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     * @throws InvocationTargetException        InvocationTargetException is a checked exception that wraps an exception thrown by an invoked method or constructor.
     * @throws NoSuchMethodException            Thrown when a particular method cannot be found.
     * @throws InstantiationException           Thrown when an application tries to create an instance of a class but the specified class object cannot be instantiated.
     * @throws IllegalAccessException           Thrown when an application tries to create an instance but does not have access to the definition of the specified class.
     */
    GameBoard(GraphicsContext GC, int numberOfTilesX, int numberOfTilesY, int numberOfCorsairs, int numberOfBuccaneers, int numberOfFilibusters, int fogOfWarRange) throws InterruptedException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        GameBoard.GC = GC;

        GameBoard.numberOfTilesX = numberOfTilesX;
        GameBoard.numberOfTilesY = numberOfTilesY;

        randMin = 0;
        randMaxX = numberOfTilesX;
        randMaxY = numberOfTilesY;

        GameBoard.fogOfWarRange = fogOfWarRange;
        fogOfWarIsEnabled = GameBoard.fogOfWarRange != -1;

        initMap();
        initPersons(numberOfCorsairs, numberOfBuccaneers, numberOfFilibusters);
        initObjects();

        System.out.println();
        selectNextAlivePlayer();

        playerTurn = true;
        listenToUserInput = true;
    }

    /**
     * Fills a certain percentage of the map to tiles of the precised class.
     * @param percentage    Percentage of tiles to fill
     * @param tileClass     Class of the tiles to create
     * @throws InvocationTargetException        InvocationTargetException is a checked exception that wraps an exception thrown by an invoked method or constructor.
     * @throws NoSuchMethodException            Thrown when a particular method cannot be found.
     * @throws InstantiationException           Thrown when an application tries to create an instance of a class but the specified class object cannot be instantiated.
     * @throws IllegalAccessException           Thrown when an application tries to create an instance but does not have access to the definition of the specified class.
     */
    private static void initTileType(double percentage, Class<?> tileClass) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        int numberOfTiles = (int) ((numberOfTilesX * numberOfTilesY) * (percentage / 100));
        while (numberOfTiles > 0) {
            int randomX = ThreadLocalRandom.current().nextInt(randMin, randMaxX);
            int randomY = ThreadLocalRandom.current().nextInt(randMin, randMaxY);
            if (map[randomY][randomX] == null) {
                Tile tile = (Tile) tileClass.getDeclaredConstructor(int.class, int.class).newInstance(randomX, randomY);
                map[randomY][randomX] = tile;
                numberOfTiles--;
            }
        }
    }

    /**
     * Initializes the map by filling it with tiles.
     * @throws InvocationTargetException        InvocationTargetException is a checked exception that wraps an exception thrown by an invoked method or constructor.
     * @throws NoSuchMethodException            Thrown when a particular method cannot be found.
     * @throws InstantiationException           Thrown when an application tries to create an instance of a class but the specified class object cannot be instantiated.
     * @throws IllegalAccessException           Thrown when an application tries to create an instance but does not have access to the definition of the specified class.
     */
    private static void initMap() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // Initialize a map with empty tiles
        map = new Tile[numberOfTilesY][numberOfTilesX];

        // Set <waterPercentage>% of tiles to water
        initTileType(waterPercentage, WaterTile.class);

        // Set <forestPercentage>% of tiles to forest
        initTileType(forestPercentage, ForestTile.class);

        // Set the remaining tiles to sand
        for (int y = 0; y < numberOfTilesY; y++) {
            for (int x = 0; x < numberOfTilesX; x++) {
                if (map[y][x] == null) {
                    map[y][x] = new SandTile(x, y);
                }
            }
        }

        // Select a random tile in the map to hide the treasure
        boolean treasureToHide = true;
        while (treasureToHide) {
            int randomX = ThreadLocalRandom.current().nextInt(randMin, randMaxX);
            int randomY = ThreadLocalRandom.current().nextInt(randMin, randMaxY);
            if (map[randomY][randomX].isDiggable) {
                map[randomY][randomX].hasTreasure = true;
                System.out.println("Treasure set to (" + randomX + ", " + randomY + ").");
                treasureToHide = false;
            }
        }
    }

    /**
     * Adds persons to the map tiles.
     * @param numberOfCorsairs      Number of corsairs (players) to add
     * @param numberOfBuccaneers    Number of buccaneers to add
     * @param numberOfFilibusters   Number of filibusters to add
     */
    private static void initPersons(int numberOfCorsairs, int numberOfBuccaneers, int numberOfFilibusters) {
        // Add the players
        int remainingPlayers = numberOfCorsairs;
        while (remainingPlayers > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn() && tile.isWalkable) {
                Corsair newPlayer = new Corsair(tile);
                tile.person = newPlayer;
                playerList.add(newPlayer);
                remainingPlayers--;
            }
        }

        // Add the buccaneers
        int remainingBuccaneers = numberOfBuccaneers;
        while (remainingBuccaneers > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn() && tile.isWalkable) {
                Buccaneer newBuccaneer = new Buccaneer(tile);
                tile.person = newBuccaneer;
                buccaneerList.add(newBuccaneer);
                remainingBuccaneers--;
            }
        }

        // Add the filibusters
        int remainingFilibusters = numberOfFilibusters;
        while (remainingFilibusters > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn() && tile.isWalkable) {
                Filibuster newFilibuster = new Filibuster(tile);
                tile.person = newFilibuster;
                filibusterList.add(newFilibuster);
                remainingFilibusters--;
            }
        }
    }

    /**
     * Adds objects to the map tiles.
     */
    private static void initObjects() {
        // Add 1 shovel per player
        int numberOfShovels = playerList.size();
        while (numberOfShovels > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn()) {
                tile.object = new Shovel(tile);
                numberOfShovels--;
            }
        }

        // Add 2 machetes per player
        int numberOfMachetes = playerList.size() * 2;
        while (numberOfMachetes > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn() && !tile.getClass().getName().equals(ForestTile.class.getName())) {
                tile.object = new Machete(tile);
                numberOfMachetes--;
            }
        }

        // Add 2 muskets per player
        int numberOfMuskets = playerList.size() * 2;
        while (numberOfMuskets > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn()) {
                tile.object = new Musket(tile);
                numberOfMuskets--;
            }
        }

        // Add 2 armors per player
        int numberOfArmors = playerList.size() * 2;
        while (numberOfArmors > 0) {
            Tile tile = getRandomTile();
            if (tile.isAvailableForSpawn()) {
                tile.object = new Armor(tile);
                numberOfArmors--;
            }
        }
    }

    /**
     * Gets a random tile from the map.
     * @return  a random tile from the map.
     */
    private static Tile getRandomTile() {
        int randomX = ThreadLocalRandom.current().nextInt(randMin, randMaxX);
        int randomY = ThreadLocalRandom.current().nextInt(randMin, randMaxY);
        return map[randomY][randomX];
    }

    /**
     * Moves the current player in the direction corresponding to the keycode.
     * @param keyCode   Code of the key pressed by the user
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void moveCurrentPlayerTo(KeyCode keyCode) throws InterruptedException, IOException {
        boolean move = true;

        if (listenToUserInput) {
            switch (keyCode) {
                case A:
                    currentPlayer.moveUp = true;
                    currentPlayer.moveLeft = true;
                    break;
                case Z:
                    currentPlayer.moveUp = true;
                    break;
                case E:
                    currentPlayer.moveUp = true;
                    currentPlayer.moveRight = true;
                    break;
                case D:
                    currentPlayer.moveRight = true;
                    break;
                case C:
                    currentPlayer.moveDown = true;
                    currentPlayer.moveRight = true;
                    break;
                case X:
                    currentPlayer.moveDown = true;
                    break;
                case W:
                    currentPlayer.moveDown = true;
                    currentPlayer.moveLeft = true;
                    break;
                case Q:
                    currentPlayer.moveLeft = true;
                    break;
                case S:
                    save(Launcher.gameBoard);
                    move = false;
                    break;
                default:
                    move = false;
                    break;
            }

            if (move && listenToUserInput) {
                currentPlayer.move();
            }
        }
    }

    /**
     * Selects the next alive player or indicate to the game to start the AIs' turn if it was the last player of the list.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void selectNextAlivePlayer() throws InterruptedException {
        checkIfThereIsStillPlayers();

        boolean playersTurnEnd = false;

        Iterator<Corsair> iterator = playerList.listIterator(currentPlayerIndex);
        if (!iterator.hasNext()) {
            playersTurnEnd = true;
        }
        else {
            do {
                currentPlayer = iterator.next();
                currentPlayerIndex++;
            } while (!currentPlayer.isAlive && iterator.hasNext());

            if (currentPlayer.isAlive) {
                Corsair.announcePlayerTurn(currentPlayer);
            }
            else {
                playersTurnEnd = true;
            }
        }

        if (playersTurnEnd) {
            playerTurn = false;
            System.out.println("## End of players' turn ##");

            currentPlayerIndex = 0;
        }

        Launcher.refreshScreen();
    }

    /**
     * Browses the players' list to end the game if all players are dead.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    private static void checkIfThereIsStillPlayers() throws InterruptedException {
        for (Corsair corsair : playerList) {
            if (corsair.isAlive)
                return;
        }
        Launcher.gameOver("All the players are dead.");
    }

    /**
     * Moves all the alive AIs.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void moveAIs() throws InterruptedException {
        // Make all Buccaneers play
        for (Buccaneer buccaneer: buccaneerList) {
            if (buccaneer.isAlive) {
                buccaneer.move();
            }
        }

        // Make all Filibuster play
        for (Filibuster filibuster: filibusterList) {
            if (filibuster.isAlive) {
                filibuster.move();
            }
        }

        System.out.println("##### End of turn " + turn + " #####\n");
        turn++;

        selectNextAlivePlayer();
        Launcher.refreshScreen();

        // Reset to the player turn
        playerTurn = true;
        listenToUserInput = true;
    }

    /**
     * Save the game in the specified file.
     * @param object    The object to save
     * @throws IOException                      Thrown when an I/O exception of some sort has occurred.
     */
    private static void save(Object object) throws IOException {
        Launcher.save(object);
    }

    /**
     * Displays the icons of every entities of the GameBoard at the correct position in the canvas.
     */
    static void display() {
        for (int y = 0; y < numberOfTilesY; y++) {
            for (int x = 0; x < numberOfTilesX; x++) {
                Tile currentTile = map[y][x];

                if (fogOfWarIsEnabled) {
                    currentTile.displayFogged();
                }
                else {
                    currentTile.display();
                }
            }
        }

        if (fogOfWarIsEnabled) {
            List<Tile> visibleTiles = GameBoard.currentPlayer.currentTile.getTilesInRange(fogOfWarRange);
            for (Tile tile : visibleTiles) {
                tile.display();
            }
        }
    }
}