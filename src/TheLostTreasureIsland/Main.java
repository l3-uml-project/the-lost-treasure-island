package TheLostTreasureIsland;

import TheLostTreasureIsland.Exceptions.ExceededLimitException;
import TheLostTreasureIsland.Exceptions.UnsupportedValueOfArgumentException;

/**
 * Class called when launching the .jar.
 */
public class Main {

    /**
     * Calls the Launcher's main method.
     * @param args  Command-line args.
     * @throws UnsupportedValueOfArgumentException     Thrown when on of the args is not correctly formed.
     * @throws ExceededLimitException           Thrown when on of the args exceeds game's limits.
     */
    public static void main(String[] args) throws UnsupportedValueOfArgumentException, ExceededLimitException {
        Launcher.main(args);
    }
}
