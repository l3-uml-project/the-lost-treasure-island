package TheLostTreasureIsland;

/**
 * Forest tile.
 */
class ForestTile extends Tile {
    /**
     * Forest tile's constructor.
     * @param xCoord    X coordinate of the tile.
     * @param yCoord    Y coordinate of the tile.
     */
    ForestTile(int xCoord, int yCoord) {
        super(xCoord, yCoord);

        icon = Icon.forest;
        foggedIcon = Icon.forestFog;

        isWalkable = false;
        isDiggable = true;
    }

    /**
     * Called when a player try to dig on this tile. Update the tile's icon.
     * @return  True if the treasure was found, false otherwise.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    boolean dig() throws InterruptedException {
        if (!isDug) {
            icon = Icon.forestDug;
            if (hasTreasure) {
                Launcher.win("You found the treasure !", (Corsair) this.person);
                return true;
            }
        }
        return false;
    }
}
