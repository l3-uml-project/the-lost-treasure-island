package TheLostTreasureIsland;

import javafx.scene.image.Image;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Person, is abstract because it only contains the basic information of the person but does not have any type.
 */
abstract class Person {
    /** Boolean referencing if the person is alive or not */
    boolean isAlive = true;

    /** Boolean referencing if the person has already attacked or not this turn */
    boolean hasAttackedThisTurn = false;

    /** Boolean referencing if the person has an armor or not */
    boolean hasArmor = false;

    /** Boolean referencing if the person has an machete or not */
    boolean hasMachete = false;

    /** Boolean referencing if the person has an musket or not */
    boolean hasMusket = false;

    /** Boolean referencing if the person has an shovel or not */
    boolean hasShovel = false;

    /** Boolean referencing if the person is trying to move in the top direction or not */
    boolean moveUp = false;

    /** Boolean referencing if the person is trying to move in the bottom direction or not */
    boolean moveDown = false;

    /** Boolean referencing if the person is trying to move in the left direction or not */
    boolean moveLeft = false;

    /** Boolean referencing if the person is trying to move in the right direction or not */
    boolean moveRight = false;

    /** ID of the person */
    int id;

    /** Pseudo of the person */
    String pseudo;

    /** Move range of the person */
    int moveRange = 1;

    /** Attack range of the person */
    int attackRange = 0;

    /** Victory percentage (chances to win a fight) of the person */
    int victoryPercentage = 0;

    /** Tile the person is currently on */
    Tile currentTile;

    /** Icon of the person */
    Image icon;

    /**
     * Person's constructor.
     * @param tile  Tile the person is currently on
     */
    Person(Tile tile) {
        currentTile = tile;
    }

    /**
     * Reset all boolean movement variables to false.
     */
    void resetMovement() {
        moveUp = false;
        moveDown = false;
        moveLeft = false;
        moveRight = false;
    }

    /**
     * Get the targeted tile (depending on boolean movement variables) from the currentTile.
     * @param currentTile   Starting tile.
     * @return  Ending tile.
     */
    Tile getTargetedTileFrom(Tile currentTile) {
        int xCoordTarget = currentTile.xCoord;
        int yCoordTarget = currentTile.yCoord;

        if (moveUp)
            yCoordTarget = Math.floorMod((yCoordTarget - 1), GameBoard.numberOfTilesY);
        if (moveDown)
            yCoordTarget = Math.floorMod((yCoordTarget + 1), GameBoard.numberOfTilesY);
        if (moveLeft)
            xCoordTarget = Math.floorMod((xCoordTarget - 1), GameBoard.numberOfTilesX);
        if (moveRight)
            xCoordTarget = Math.floorMod((xCoordTarget + 1), GameBoard.numberOfTilesX);

        return GameBoard.map[yCoordTarget][xCoordTarget];
    }

    /**
     * Attack method used to randomly determine if one of the fighter has died in the fight or not.
     * @param defender  Attacked person.
     * @return  True if the defender was killed and the person can move on its tile, false otherwise.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    boolean attack(Person defender) throws InterruptedException {
        // Verify that the battle is Corsair VS AI (or AI VS Corsair)
        if (!isCorsairVsAiBattle(this, defender))
            return false;

        System.out.println("- " + this.pseudo + " is attacking " + defender.pseudo);
        hasAttackedThisTurn = true;

        // If the attacker has a higher victory percentage than the defender
        if (this.victoryPercentage >= defender.victoryPercentage) {
            int killChances = this.victoryPercentage - defender.victoryPercentage;
            int randomNumber = ThreadLocalRandom.current().nextInt(1, 101); // Random number between 1 and 100

            if (randomNumber <= killChances) {
                defender.die();
                displayKillMessage(this, defender, killChances);
                return true;
            }
            else {
                displayDodgeMessage(defender, this, 100 - killChances);
            }
        }

        // If the defender has a higher victory percentage than the attacker
        else {
            int killChances = defender.victoryPercentage - this.victoryPercentage;
            int randomNumber = ThreadLocalRandom.current().nextInt(1, 101); // Random number between 1 and 100

            if (randomNumber <= killChances) {
                this.die();
                displayKillMessage(defender, this, killChances);
                return false;
            }
            else {
                displayDodgeMessage(this, defender, 100 - killChances);
            }
        }

        return false;
    }

    /**
     * Finds whether the battle involves a player and an AI or not.
     * @param attacker  Attacking person
     * @param defender  Defending person
     * @return  True if the battle is of the type 'PlayerVsAI', false otherwise.
     */
    private static boolean isCorsairVsAiBattle(Person attacker, Person defender) {
        String attackerClass = attacker.getClass().getName();
        String defenderClass = defender.getClass().getName();
        String corsair = Corsair.class.getName();

        // If the attacker is a Corsair (player)
        if (attackerClass.equals(corsair)) {
            // And the defender is a Corsair (player)
            if (defenderClass.equals(corsair))
                return false;
            else
                return true;
        }

        // If the attacker is NOT a Corsair (-> AI)
        else {
            // And the defender is NOT a Corsair (-> AI)
            if (!defenderClass.equals(corsair))
                return false;
            else
                return true;
        }
    }

    /**
     * Called when a person has died.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    private void die() throws InterruptedException {
        isAlive = false;
        currentTile.person = null;
        currentTile.hasSkull = true;
        Launcher.refreshScreen();
    }

    /**
     * Recalculates the person's victory percentage depending on the objects he has.
     * @param person    Person to do the math on
     */
    static void recalculateVictoryPercentage(Person person) {
        if (person.hasMusket) {
            if (person.hasArmor)
                person.victoryPercentage = 100;
            else
                person.victoryPercentage = 90;
        }
        else if (person.hasMachete) {
            if (person.hasArmor)
                person.victoryPercentage = 50;
            else
                person.victoryPercentage = 40;
        }
        else if (person.hasArmor) {
            person.victoryPercentage = 20;
        }
        else {
            person.victoryPercentage = 10;
        }
    }

    /**
     * Displays in the console a message informing that a person has killed another.
     * @param killer        Person that killed.
     * @param killed        Person that was killed.
     * @param percentage    Percentage of chances that the killer had to kill the other person.
     */
    private static void displayKillMessage(Person killer, Person killed, int percentage) {
        System.out.println("-- " + killer.pseudo + " killed " + killed.pseudo + " ! (" + percentage + "% chance)");
    }

    /**
     * Displays in the console a message informing that a person has dodged another person's attack.
     * @param defender        Person that dodged this attack.
     * @param attacker        Person that attacked.
     * @param percentage    Percentage of chances that the defender had to dodge the other person's attack.
     */
    private static void displayDodgeMessage(Person defender, Person attacker, int percentage) {
        System.out.println("-- " + defender.pseudo + " dodged " + attacker.pseudo + "'s attack ! (" + percentage + "% chance)");
    }

    /**
     * Displays the person's icon at the current position of the canvas.
     */
    void display() {
        GameBoard.GC.drawImage(icon, currentTile.xCoord * GameBoard.TILE_SIZE, currentTile.yCoord * GameBoard.TILE_SIZE);
    }
}