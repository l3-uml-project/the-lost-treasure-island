package TheLostTreasureIsland;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * AI, is abstract because it only contains the methods of the AIs but does not have any type.
 */
abstract class AI extends Person {
    /**
     * AI's constructor.
     * @param tile  Tile the AI is currently on
     */
    AI(Tile tile) {
        super(tile);
    }

    /**
     * Sets some boolean movements variables to true based on a random number.
     */
    private void setRandomMove() {
        int randomMove = ThreadLocalRandom.current().nextInt(0, 8);

        switch (randomMove) {
            case 0:
                moveUp = true;
                break;
            case 1:
                moveUp = true;
                moveRight = true;
            case 2:
                moveRight = true;
                break;
            case 3:
                moveDown = true;
                moveRight = true;
            case 4:
                moveDown = true;
                break;
            case 5:
                moveDown = true;
                moveLeft = true;
            case 6:
                moveLeft = true;
                break;
            case 7:
                moveUp = true;
                moveLeft = true;
        }
    }

    /**
     * Main move method used to make an AI do everything it cans in its turn (fight, move, ...).
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    void move() throws InterruptedException {
        System.out.println(pseudo + "'s turn");

        hasAttackedThisTurn = false;

        int randomNumberOfMoves = ThreadLocalRandom.current().nextInt(1, moveRange + 1); // Random number between 1 and moveRange
        for (int currentMoves = 0; currentMoves < randomNumberOfMoves; currentMoves++) {
            boolean moved = false;
            while (!moved) {
                moved = moveOnce();
            }
        }
    }

    /**
     * Main method used to make an AI do everything it cans in one move (fight, move, ...).
     * @return  True if we can consider that the player has moved, else otherwise.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    private boolean moveOnce() throws InterruptedException {
        if (!isAlive)
            return true;

        setRandomMove();
        Tile targetedTile = getTargetedTileFrom(currentTile);

        resetMovement();

        // Cancel the move if the tile is a water tile or a forest tile and the player don't have a Machete
        if (!targetedTile.isWalkable)
            if (!(targetedTile.getClass().getName().equals(ForestTile.class.getName()) && hasMachete))
                return false;

        // Attack if there is someone on the targeted tile
        if (targetedTile.person != null) {
            boolean enemyKilled = attack(targetedTile.person);
            if (!enemyKilled) {
                Launcher.refreshScreen();
                return true;
            }
        }

        // Move to the targeted tile
        currentTile.person = null;
        currentTile = targetedTile;
        currentTile.person = this;

        // If there was no one on the targeted tile, try to attack a distant person (if attackRange > 0)
        if (!hasAttackedThisTurn) {
            Person randomPersonInRange = getRandomPersonToAttack();
            if (randomPersonInRange != null) {
                boolean enemyKilled = attack(randomPersonInRange);
                if (!enemyKilled) {
                    if (!isAlive)
                        Launcher.refreshScreen();
                    return true;
                }
            }
        }

        Launcher.refreshScreen();

        return true;
    }

    /**
     * Gets a random person to attack depending on the person's attack range.
     * @return  A targeted person.
     */
    private Person getRandomPersonToAttack() {
        List<Person> personsInRange = getPersonsInRange();

        if(!personsInRange.isEmpty()) {
            int randomNumber = ThreadLocalRandom.current().nextInt(0, personsInRange.size());
            return personsInRange.get(randomNumber);
        }

        return null;
    }

    /**
     * Gets all the persons in range of attack (depending on the person's attack range).
     * @return  A list of the persons in range of attack.
     */
    private List<Person> getPersonsInRange() {
        List<Tile> tilesInRange = currentTile.getTilesInRange(attackRange);
        List<Person> personsInRange = new ArrayList<>();

        for (Tile tile : tilesInRange) {
            if (tile.person != null && tile.person != this) {
                personsInRange.add(tile.person);
            }
        }

        return personsInRange;
    }
}
