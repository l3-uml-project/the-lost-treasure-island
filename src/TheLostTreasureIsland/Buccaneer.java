package TheLostTreasureIsland;

/**
 * Buccaneer (AI).
 */
class Buccaneer extends AI {
    /** Number of created buccaneers. Increments by 1 every time a buccaneer is created */
    private static int buccaneerID = 1;

    /**
     * Buccaneer's constructor.
     * @param tile  Tile the buccaneer is currently on
     */
    Buccaneer(Tile tile) {
        super(tile);

        id = buccaneerID;
        buccaneerID++;

        pseudo = "Buccaneer " + id;

        hasMachete = true;
        recalculateVictoryPercentage(this);

        moveRange = 2;
        attackRange = 0;

        icon = Icon.buccaneer;
    }
}
