package TheLostTreasureIsland;

import javafx.scene.image.Image;

/**
 * Game object, is abstract because it only contains the basic information of the game object but does not have any type.
 */
abstract class GameObject {
    /** Tile the object is currently on */
    Tile currentTile;

    /** X coordinate of the object */
    private int xCoord;

    /** Y coordinate of the object */
    private int yCoord;

    /** Icon of the object */
    Image icon;

    /**
     * Game object's constructor.
     * @param tile  Tile the object is currently on
     */
    GameObject(Tile tile) {
        currentTile = tile;

        xCoord = tile.xCoord;
        yCoord = tile.yCoord;
    }

    /**
     * Displays the object's icon at the current position of the canvas.
     */
    void display() {
        GameBoard.GC.drawImage(icon, xCoord * GameBoard.TILE_SIZE, yCoord * GameBoard.TILE_SIZE);
    }
}
