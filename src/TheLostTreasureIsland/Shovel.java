package TheLostTreasureIsland;

/**
 * Shovel (game object).
 */
class Shovel extends GameObject {
    /**
     * Shovel's constructor.
     * @param tile  Tile the shovel is currently on
     */
    Shovel(Tile tile) {
        super(tile);
        icon = Icon.shovel;
    }
}
