package TheLostTreasureIsland;

/**
 * Corsair (player).
 */
class Corsair extends Person {
    /** Number of created corsairs. Increments by 1 every time a corsair is created */
    private static int corsairID = 1;

    /** Array containing the possible colors of the corsairs */
    private static String[] corsairColors = new String[8];

    /**
     * Corsair's constructor.
     * @param tile  Tile the corsair is currently on.
     */
    Corsair(Tile tile) {
        super(tile);

        id = corsairID;
        corsairID++;

        pseudo = "Player " + id;

        recalculateVictoryPercentage(this);

        moveRange = 2;
        attackRange = 0;

        icon = Icon.corsairs[id - 1];
    }

    /**
     * Fills the corsairColors array with every corsair's possible colors.
     */
    static void init() {
        corsairColors[0] = "Blue";
        corsairColors[1] = "Red";
        corsairColors[2] = "Green";
        corsairColors[3] = "Magenta";
        corsairColors[4] = "Cyan";
        corsairColors[5] = "Yellow";
        corsairColors[6] = "White";
        corsairColors[7] = "Black";
    }

    /**
     * Main move method used to make a player do everything it cans in its turn (fight, move, pick up an object, dig, ...).
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    void move() throws InterruptedException {
        GameBoard.listenToUserInput = false;

        Tile targetedTile = getTargetedTileFrom(currentTile);

        resetMovement();

        // Cancel the move if the tile is a water tile or a forest tile and the player don't have a Machete
        if (!targetedTile.isWalkable) {
            if (!(targetedTile.getClass().getName().equals(ForestTile.class.getName()) && hasMachete)) {
                GameBoard.listenToUserInput = true;
                return;
            }
        }

        // Attack if there is someone on the targeted tile
        if (targetedTile.person != null) {
            boolean enemyKilled = attack(targetedTile.person);
            if (!enemyKilled) {
                if (!isAlive)
                    Launcher.refreshScreen();
                endTheMove();
                return;
            }
        }

        // Move to the targeted tile
        currentTile.person = null;
        currentTile = targetedTile;
        currentTile.person = this;

        // Pick up the object if there is any on the targeted tile
        if (targetedTile.object != null)
            pickUp();

        // Dig if the corsair has a shovel
        if (hasShovel)
            if (dig())
                return;

        Launcher.refreshScreen();
        endTheMove();

    }

    /**
     * Called in the corsair's move method when it's the end of its turn.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    private static void endTheMove() throws InterruptedException {
        GameBoard.selectNextAlivePlayer();

        // Launch AI's turn if all players have finished their turn
        if (!GameBoard.playerTurn) {
            GameBoard.moveAIs();
        }
        else {
            GameBoard.listenToUserInput = true;
        }
    }

    /**
     * Method allowing the player to pick up an object if there is one on its current tile.
     */
    private void pickUp() {
        // Armor
        if (currentTile.object.getClass().getName().equals(Armor.class.getName()) && !hasArmor) {
            hasArmor = true;
            currentTile.object.currentTile = null;
            currentTile.object = null;
            System.out.println("- Player " + id + " picked up an Armor.");
            recalculateVictoryPercentage(this);
        }

        // Machete
        else if (currentTile.object.getClass().getName().equals(Machete.class.getName()) && !hasMachete) {
            hasMachete = true;
            currentTile.object.currentTile = null;
            currentTile.object = null;
            System.out.println("- Player " + id + " picked up a Machete.");
            recalculateVictoryPercentage(this);
        }

        // Musket
        else if (currentTile.object.getClass().getName().equals(Musket.class.getName()) && !hasMusket) {
            hasMusket = true;
            currentTile.object.currentTile = null;
            currentTile.object = null;
            System.out.println("- Player " + id + " picked up a Musket");
            recalculateVictoryPercentage(this);
        }

        // Shovel
        else if (currentTile.object.getClass().getName().equals(Shovel.class.getName()) && !hasShovel) {
            hasShovel = true;
            currentTile.object.currentTile = null;
            currentTile.object = null;
            System.out.println("- Player " + id + " picked up a Shovel.");
        }
    }

    /**
     * Method allowing the player to dig to try to find the treasure on its current tile.
     * @return  True if the treasure was found, false otherwise.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    private boolean dig() throws InterruptedException {
        return currentTile.dig();
    }

    /**
     * Method used to announce the current player's turn.
     * @param currentPlayer
     */
    static void announcePlayerTurn(Corsair currentPlayer) {
        System.out.println("Player " + currentPlayer.id + " (" + Corsair.corsairColors[currentPlayer.id - 1] + ")'s turn ! (shovel:" + currentPlayer.hasShovel + ", machete:" + currentPlayer.hasMachete + ", victory percentage:" + currentPlayer.victoryPercentage + "%)");
    }
}
