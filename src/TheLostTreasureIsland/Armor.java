package TheLostTreasureIsland;

/**
 * Armor (game object).
 */
class Armor extends GameObject {
    /**
     * Armor's constructor.
     * @param tile  Tile the armor is currently on
     */
    Armor(Tile tile) {
        super(tile);
        icon = Icon.armor;
    }
}
