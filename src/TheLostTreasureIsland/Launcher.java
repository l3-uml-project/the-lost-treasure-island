package TheLostTreasureIsland;

import TheLostTreasureIsland.Exceptions.ExceededLimitException;
import TheLostTreasureIsland.Exceptions.UnsupportedValueOfArgumentException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Main class of the game.
 */
public class Launcher extends Application {
    /** Application root */
    private static Group root = new Group();

    /** Main scene */
    private static Scene scene = new Scene(root);

    /** Stage used to display the game */
    private static Stage staticStage;

    /** GameBoard object that contains all the information of the game */
    static GameBoard gameBoard;

    /** Size of the GameBoard */
    private static int size = 1;

    /** Number of tiles per raw */
    private static int numberOfTilesX;

    /** Number of tiles per column */
    private static int numberOfTilesY;

    /** Tile size in pixels */
    private static int TILE_SIZE = 32;

    /** Range of the fog of war */
    private static int fogOfWarRange = -1;

    /** Number of corsairs (players) at the start of the game */
    private static int numberOfCorsairs = 1;

    /** Array containing corsairs number limits */
    private static int[] corsairsLimits = new int[3];

    /** Number of buccaneers at the start of the game */
    private static int numberOfBuccaneers = 0;

    /** Number of filibusters at the start of the game */
    private static int numberOfFilibusters = 0;

    /** Array containing enemies number limits */
    private static int[] enemyLimit = new int[3];

    /** Saves path */
    private static final String SAVES_PATH = "saves";

    /** Name of the save to load */
    private static String saveName = "";

    /** Boolean that references if the game should be loaded from a save file or not */
    private static boolean doLoad = false;

    /**
     * Initializes the game.
     * @param args  Command-line args.
     * @throws UnsupportedValueOfArgumentException     Thrown when on of the args is not correctly formed.
     * @throws ExceededLimitException           Thrown when on of the args exceeds game's limits.
     */
    private static void init(String[] args) throws UnsupportedValueOfArgumentException, ExceededLimitException {
        // Init the Icon class (corsair icons)
        Icon.init();

        // Init the Corsair class (corsair colors)
        Corsair.init();

        corsairsLimits[0] = 2;
        corsairsLimits[1] = 4;
        corsairsLimits[2] = 8;

        enemyLimit[0] = 5;
        enemyLimit[1] = 10;
        enemyLimit[2] = 20;

        for (String arg: args) {
            String[] currentArg = arg.split(":", 2);
            String param = currentArg[0].toLowerCase();
            String value = currentArg[1];

            boolean enemy = false;

            switch (param) {
                // SIZE
                case "size":
                case "s":
                    try {
                        size = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        throw new UnsupportedValueOfArgumentException("size", value);
                    }
                    break;

                // CORSAIRS
                case "corsair":
                case "corsairs":
                case "c":
                    try {
                        numberOfCorsairs = Integer.parseInt(value);
                        if (numberOfCorsairs > corsairsLimits[size - 1])
                            throw new ExceededLimitException(param, numberOfCorsairs, corsairsLimits[size - 1]);
                        if (numberOfCorsairs < 1)
                            throw new UnsupportedValueOfArgumentException("corsairs", value);
                    } catch (NumberFormatException e) {
                        throw new UnsupportedValueOfArgumentException("corsairs", value);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        throw new UnsupportedValueOfArgumentException("size", Integer.toString(size));
                    }
                    break;

                // BUCCANEERS
                case "buccaneer":
                case "buccaneers":
                case "b":
                    enemy = true;
                    try {
                        numberOfBuccaneers = Integer.parseInt(value);
                        if (numberOfBuccaneers < 0)
                            throw new UnsupportedValueOfArgumentException("buccaneers", value);
                    } catch (NumberFormatException e) {
                        throw new UnsupportedValueOfArgumentException("buccaneers", value);
                    }
                    break;

                // FILIBUSTERS
                case "filibuster":
                case "filibusters":
                case "f":
                    enemy = true;
                    try {
                        numberOfFilibusters = Integer.parseInt(value);
                        if (numberOfBuccaneers < 0)
                            throw new UnsupportedValueOfArgumentException("filibusters", value);
                    } catch (NumberFormatException e) {
                        throw new UnsupportedValueOfArgumentException("filibusters", value);
                    }
                    break;

                case "fog":
                    fogOfWarRange = Integer.parseInt(value);
                    break;

                case "save":
                    saveName = value;
                    break;

                case "load":
                    if (value.equals("true"))
                        doLoad = true;
                    break;
            }

            // ENEMY
            if (enemy) {
                try {
                    if (numberOfBuccaneers + numberOfFilibusters > enemyLimit[size - 1])
                        throw new ExceededLimitException("buccaneers + filibusters", numberOfBuccaneers + numberOfFilibusters, enemyLimit[size - 1]);
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new UnsupportedValueOfArgumentException("size", Integer.toString(size));
                }
            }

            // Specify a generic save file if none was specified
            if (saveName.isEmpty()) {
                saveName = "save";
            }
        }

        switch (size) {
            case 1:
                numberOfTilesX = 5;
                numberOfTilesY = 5;
                break;
            case 2:
                numberOfTilesX = 10;
                numberOfTilesY = 10;
                break;
            case 3:
                numberOfTilesX = 20;
                numberOfTilesY = 20;
                break;
            default:
                throw new UnsupportedValueOfArgumentException("size", Integer.toString(size));
        }
    }

    /**
     * Adds the game's key events.
     */
    private static void addKeyEvents() {
        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                System.out.println("Exiting to desktop.");
                Platform.exit();
            } else {
                // Prepare a thread for the calculations
                Task task = new Task<Void>() {
                    @Override
                    public Void call() throws InterruptedException, IOException {
                        GameBoard.moveCurrentPlayerTo(event.getCode());
                        return null;
                    }
                };

                new Thread(task).start();
            }
        });
    }

    /**
     * Resets the game's key events at the end of the game to prevent the user to try to continue playing.
     * <p>
     *     The only remaining key is 'Escape' and allows the user to close the application.
     * </p>
     */
    private static void resetKeyEvents() {
        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                System.out.println("Exiting to desktop.");
                Platform.exit();
            }
        });
    }

    /**
     * Main method called by the application.
     * @param stage     Stage used to display the game
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     * @throws InvocationTargetException        InvocationTargetException is a checked exception that wraps an exception thrown by an invoked method or constructor.
     * @throws NoSuchMethodException            Thrown when a particular method cannot be found.
     * @throws InstantiationException           Thrown when an application tries to create an instance of a class but the specified class object cannot be instantiated.
     * @throws IllegalAccessException           Thrown when an application tries to create an instance but does not have access to the definition of the specified class.
     */
    public void start(Stage stage) throws InterruptedException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException, ClassNotFoundException {
        staticStage = stage;

        staticStage.setTitle("The Lost Treasure Island");
        staticStage.setScene(scene);

        Canvas canvas = new Canvas(numberOfTilesX * TILE_SIZE, numberOfTilesY * TILE_SIZE);
        root.getChildren().add(canvas);

        GraphicsContext GC = canvas.getGraphicsContext2D();

        if (doLoad) {
            gameBoard = (GameBoard) load();
            System.out.println("Loaded " + saveName + ".");
        }

        if (gameBoard == null) {
            gameBoard = new GameBoard(GC, numberOfTilesX, numberOfTilesY, numberOfCorsairs, numberOfBuccaneers, numberOfFilibusters, fogOfWarRange);
        }
        GameBoard.display();

        refreshScreen();

        addKeyEvents();
    }

    /**
     * Displays a 'GameOver' message in the console and draw GameOver on the screen.
     * @param message   Message displayed in the console.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void gameOver(String message) throws InterruptedException {
        System.out.println(message);
        System.out.println("### GAME OVER ###");
        resetKeyEvents();
        refreshScreen();
        drawGameOver();
    }

    /**
     * Displays a 'You Win' message in the console and draw GameOver on the screen.
     * @param message   Message displayed in the console.
     * @param player    Winner of the game.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void win(String message, Corsair player) throws InterruptedException {
        System.out.println(message);
        System.out.println("### PLAYER " + player.id + " WINS ###");
        resetKeyEvents();
        refreshScreen();
        drawGameOver();
    }

    /**
     * Save an object and its components to a file.
     * @param object    The object to save
     * @throws IOException                      Thrown when an I/O exception of some sort has occurred.
     */
    static void save(Object object) throws IOException {
        DataManager.saveToFile(object, SAVES_PATH, saveName);
    }

    /**
     * Load an objects and its components from a file.
     * @return  The loaded object
     * @throws IOException                      Thrown when an I/O exception of some sort has occurred.
     * @throws ClassNotFoundException           Thrown when an application tries to load in a class but no definition for the class with the specified name could be found.
     */
    static Object load() throws IOException, ClassNotFoundException {
        return DataManager.loadFromFile(SAVES_PATH, saveName);
    }

    /**
     * Draws the 'GameOver' image on the screen.
     */
    private static void drawGameOver() {
        int xCenter = (GameBoard.numberOfTilesX * GameBoard.TILE_SIZE) / 2;
        int yCenter = (GameBoard.numberOfTilesY * GameBoard.TILE_SIZE) / 2;

        int xOffSet = (int) Icon.gameOver.getWidth() / 2;
        int yOffSet = (int) Icon.gameOver.getHeight() / 2;

        GameBoard.GC.drawImage(Icon.gameOver, xCenter - xOffSet, yCenter - yOffSet);
        staticStage.show();
    }

    /**
     * Refreshes the screen on the JavaFX Application Thread.
     * @throws InterruptedException             Thrown when an active thread is interrupted.
     */
    static void refreshScreen() throws InterruptedException {
        Platform.runLater(() -> {
            GameBoard.display();
            staticStage.show();
        });
        Thread.sleep(300);
    }

    /**
     * Calls the Launcher's main method.
     * @param args  Command-line args.
     * @throws UnsupportedValueOfArgumentException     Thrown when on of the args is not correctly formed.
     * @throws ExceededLimitException           Thrown when on of the args exceeds game's limits.
     */
    public static void main(String[] args) throws UnsupportedValueOfArgumentException, ExceededLimitException {
        init(args);

        System.out.println("size: " + size);
        System.out.println("corsairs: " + numberOfCorsairs + "/" + corsairsLimits[size - 1]);
        System.out.println("buccaneers: " + numberOfBuccaneers + "/" + enemyLimit[size - 1]);
        System.out.println("filibusters: " + numberOfFilibusters + "/" + enemyLimit[size - 1]);
        System.out.println("total of enemies: " + (numberOfBuccaneers + numberOfFilibusters) + "/" + enemyLimit[size - 1]);

        launch(args);
    }
}