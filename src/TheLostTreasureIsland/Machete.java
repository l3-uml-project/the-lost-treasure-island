package TheLostTreasureIsland;

/**
 * Machete (game object).
 */
class Machete extends GameObject {
    /**
     * Machete's constructor.
     * @param tile  Tile the machete is currently on
     */
    Machete(Tile tile) {
        super(tile);
        icon = Icon.machete;
    }
}
