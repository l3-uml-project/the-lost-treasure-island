package TheLostTreasureIsland;

/**
 * Musket (game object).
 */
class Musket extends GameObject {
    /**
     * Musket's constructor.
     * @param tile  Tile the musket is currently on
     */
    Musket(Tile tile) {
        super(tile);
        icon = Icon.musket;
    }
}
