package TheLostTreasureIsland;

/**
 * Water tile.
 */
class WaterTile extends Tile {
    /**
     * Water tile's constructor.
     * @param xCoord    X coordinate of the tile.
     * @param yCoord    Y coordinate of the tile.
     */
    WaterTile(int xCoord, int yCoord) {
        super(xCoord, yCoord);

        icon = Icon.water;
        foggedIcon = Icon.waterFog;

        isWalkable = false;
    }
}
