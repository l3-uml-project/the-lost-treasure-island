# The Lost Treasure Island

## [FR] Summary

> L’objectif  de  ce  projet  est  de  mettre  en  place  une  application  informatique  au  moyen  d’approches issues du génie logiciel, et en particulier au travers d’une modélisation orientée objet.
> Vous devrez ainsi définir un planning de travail, avec une estimation de l’organisation de votre temps. Puis vous devrez  travailler  à  la  réalisation  de  différents  diagrammes  UML  (listés  dans  la  suite  de  ce document). Enfin, vous proposerez une implémentation de votre diagramme de classe permettant de réaliser le logiciel qui vous aura été demandé.

## Quick description

This is a 2D-tilemap game where the player(s) (corsairs) have to find the treasure that is hidden on the map.

### Pirates

To stop the players in their run, pirates (controlled by the AI) will try to kill them.
There are 2 types of pirates:
- Buccaneers: close-combat units moving 1 or 2 tiles each turn;
- Filibusters: ranged units that can fire on a player that is next to them, but can only move 1 tile per turn.

### Fights

By default, you have 10% chances of winning a fight.
Buccaneers have 40% and filibusters have 90%. It means than if a fight occurs between a buccaneers and you, you'll have (by default) 40% - 10% = 30% chances of dying (for filibuster: 90% - 10% = 80%).

### Objects

To increase your chances of winning (and not dying) in a fight, you can collect objects.

Objects are scattered on the map:
- Shovel: essential object to win, allows you to dig to try to find the treasure. There is 1 per player on the map;
- Machete: allows to move on forest tiles, and increase chances of winning a fight **to** 40%. There are 2 per player in the map;
- Musket: increase the chances of winning a fight **to** 90%. There are 2 per player in the map;
- Armor: increase the chances of winning a fight **by** 10%. There are 2 per player in the map;

Machete and Musket effects does not add up. It means that if you have a Musket, you'll have 90% chances of winning a fight weather or not you have a Machete.

But Armor's effect does add up:
- Armor + Machete = 50% chances;
- Armor + Musket = 100% chances;

> **Note 1:** The calculation is still done for each fight:
Fighting a filibuster will give you 100% - 90% = 10% chances of killing him.

> **Note 2:** As long as your percentage of winning a fight is superior to the enemy's one, you **can't** die in that fight.

## Parameters of the game

The parameters of the game are the following:

- "**size**" or "s": Set the size of the map.
    - **1** = 5x5;
    - **2** = 10x10;
    - **3** = 20x20.
- "**corsair**", "corsairs" or "c": Set the number of corsairs (players) on the map. **Max** is:
    - **2** for size 1;
    - **4** for size 2;
    - **8** for size 3;
- "**buccaneer**", "buccaneers" or "b": The number of buccaneers on the map.
- "**filibuster**", "filibusters" or "f": The number of filibusters on the map.
- "**fog**": Set the range of the fog of war for the game.

> **Note 1:** Buccaneers and filibusters count as (AIs/Pirates/Enemies). Their (total) number is limited to:
> - **5** for size 1;
> - **10** for size 2;
> - **20** for size 3;
> 
> For example, it means that starting a game with 3 buccaneers and 3 filibusters on a size 1 map won't launch the game.

> **Note 2:** None of the parameters are essential and their order is not important. They are (by default) set to:
> - size:1
> - corsair:1
> - buccaneer:0
> - filibuster:0
> - fog:-1

> **Note 3:** fog:-1 will disable the fog of war.

### Examples:

Examples of parameters sets when launching a game:
- ``size:3 corsair:5 buccaneer:6 filibuster:4 fog:3``
- ``size:2 corsair:2 filibuster:10``
- ``filibuster:1 buccaneer:3 fog:1``

## How to play

When the game is launched, press (A, Z, E, D, C, X, W or Q) key to move in this direction.

Collect objects and fight with AIs by moving on the tile they are on.

Once you have a shovel, you automatically dig after you moved.

## Requirements

Java SDK 11.0.3 and JavaFX were used to make this game.